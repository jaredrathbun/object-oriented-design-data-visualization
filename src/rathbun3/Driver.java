package rathbun3;

/**
 * <pre>
 * Author: Jared Rathbun 
 * Course: CSC 2620 
 * Due Date: 10/7/2020 
 * 
 * This class creates a Model and ViewController which displays the GUI for the
 * project.
 * </pre>
 */
public class Driver
{
    public static void main(String[] args)
    {
        // Create a new ViewController object.
        new ViewController(new Model(), "Data Visualization");
    }
    
}
