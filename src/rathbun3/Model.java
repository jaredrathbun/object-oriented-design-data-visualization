package rathbun3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.TreeSet;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

/**
 * <pre>
 * Author: Jared Rathbun 
 * Course: CSC 2620 
 * Due Date: 10/7/2020
 *
 * This class holds the data for a Model, which creates a set of data from the 
 * data the user enters to the program.
 * </pre>
 */
public class Model
{
    private LinkedHashMap<String, ArrayList<Point>> dataMap;
    private File file = null; 
    private final ArrayList<String> xVals;
    private final TreeSet<Integer> yVals; 
    
    /**
     * Constructor that initializes the Data Structures used in the class.
     */
    public Model()
    {
        dataMap = new LinkedHashMap<>();
        xVals = new ArrayList<>();
        yVals = new TreeSet<>();
    } 
    
    /**
     * Getter method for the LinkedHashMap.
     * 
     * @return The HashMap of data the user entered.
     */
    public HashMap<String, ArrayList<Point>> getDataMap() 
    {
        return dataMap;
    }
    
    /**
     * Getter method for the name of the file.
     * 
     * @return The filename of the File object used to read the .csv file. 
     */
    public String getFilename()
    {
        return file.getName();
    }
    
    /**
     * This method reads the .csv file entered into the program via a 
     * JFileChooser.
     */
    public void readFile()
    {          
        BufferedReader br = null; // A BufferedReader to read the file.
        String line; // A String object for each line of the file.
        
        // Allocate a JFileChooser object.
        JFileChooser display = new JFileChooser();
        
        // Set the dialog title.
        display.setDialogTitle("File Selection");
        
        // Set the filters so just .csv's are accepted.
        display.setFileFilter(new FileNameExtensionFilter("CSV File (.csv)", 
                "csv"));
        
        // Prompt the user for the .csv file.
        if (display.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
            file = display.getSelectedFile();
        
        /**
         * Try to read the .csv file the user selected. If the operation throws 
         * an IOException or FileNotFoundException, it is caught and displayed 
         * to the user.
         */
        try 
        {
            try
            {
                br = new BufferedReader(new FileReader(file));

                // Read the first line of the file. 
                String[] firstLineArray = br.readLine().split(",");
                
                // An array for the current line.
                String[] lineArray;
                
                /* If the first line is longer than 2 entries, do a certain 
                type of reading on it; otherwise, read the other type of 
                file. */
                if (firstLineArray.length > 2)
                {
                    // Add the X-Values to an ArrayList for GUI components.
                    for (int i = 1; i < firstLineArray.length; i++)
                        xVals.add(firstLineArray[i]);

                    /* While there is a next line to read, take each entry and 
                    create a new Point object using the year (x) and number of 
                    years (y). */
                    while ((line = br.readLine()) != null)
                    {
                        // Capture the line.
                        lineArray = line.split(",");
                        
                        // An ArrayList for the point on each band.
                        ArrayList<Point> pointList = null;
                        
                        /* Loop over every element in the line and create a 
                        new point, then add it to the HashMap. */
                        for (int i = 1; i < lineArray.length; i++)
                        {
                            String key = firstLineArray[i];
                            
                            int y = Integer.parseInt(lineArray[i]);
                            
                            /* If the ArrayList has been created, add the point.
                            If the ArrayList hasn't been created, create it, 
                            then add the point.
                            */
                            if (dataMap.get(key) != null)
                            {
                                pointList = dataMap.get(key);
                                pointList.add(new Point(lineArray[0], y));
                                dataMap.put(key, pointList);
                                
                                // Add the Y-Values to the ArrayList.
                                yVals.add(y);
                            }
                            else
                            {
                                dataMap.put(key, new ArrayList<>());
                                pointList = dataMap.get(key);
                                pointList.add(new Point(lineArray[0], y));
                                dataMap.put(key, pointList);
                            }
                                                               
                        }
                        
                        /* Reassign the pointList to null so that it can be 
                        recreated. */
                        pointList = null;
                    }   
                }
                else
                {
                    br.readLine();
                    
                    /* While there is a next line to read, read the line and 
                    add it to the HashMap. */
                    while ((line = br.readLine()) != null)
                    {
                        // An ArrayList for the points on each line.
                        ArrayList<Point> pointList = new ArrayList<>();
                        
                        // Capture the line.
                        lineArray = line.split(",");
                        
                        // The first element is the key.
                        String key = lineArray[0];
                        
                        // The second element is the number of books, or y val.
                        int numOfBooks = Integer.valueOf(lineArray[1]);
                        
                        pointList.add(new Point(key, numOfBooks));
                        dataMap.put(key, pointList);
                        xVals.add(lineArray[0]);
                        yVals.add(Integer.valueOf(lineArray[1]));
                    }
                }
            }
            catch (NullPointerException e)
            {
                System.err.println("Null Pointer Exception during file "
                        + "reading.");
                System.err.println(e.getMessage());
            }
        }
        catch (FileNotFoundException e)
        {
            JOptionPane.showMessageDialog(null, "Error reading file. \n"
                            + "Info: FileNotFoundException");
            System.err.println(e.getMessage());
        } catch (IOException e)
        {
            JOptionPane.showMessageDialog(null, "Error reading file. \n"
                            + "Info: IOException");
            System.err.println(e.getMessage());
        }
        finally
        {
            if (br != null)
            {
                try 
                {
                    br.close();
                } 
                catch (IOException e) 
                {
                    JOptionPane.showMessageDialog(null, "Error reading file. \n"
                            + "Info: IOException");
                    System.err.println(e.getMessage());
                }
            }
        }
    }
    
    /**
     * This method saves a chart as a .png image.
     * 
     * @param saveFile The file to save to.
     * @param chart The chart to save.
     * @return true if the operation was successful, false if it wasn't.
     */
    public boolean saveImage(File saveFile, JFreeChart chart)
    {
        assert (saveFile != null) && (chart != null);
        
        try
        {
            ChartUtilities.saveChartAsPNG(saveFile, chart, 850, 850);
        } catch (FileNotFoundException e)
        {
            System.err.println("Saving Failed - Reason: FileNotFoundException");
            return false;
        } catch (IOException e)
        {
            System.err.println("Saving Failed - Reason: IOException");
            return false;
        }
        
        return true;
    }   
    
    /**
     * This method creates an Array of X-Values is are sorted and cannot 
     * contain duplicates.
     * 
     * @return A sorted, non-duplicated, array of Strings containing the 
     * X-Values of the Points. 
     */
    public String[] getXValues()
    {  
       // Convert the TreeMap of X-Values to an array of Objects.
       Object[] xValsToArray = xVals.toArray();
       
       // Create a new Array of Strings that is the same size as xValsToArray.
       String[] xValsArr = new String[xVals.size()];
        
       // Convert each object in xValsToArray to a String.
       for (int i = 0; i < xVals.size(); i++)
           xValsArr[i] = String.valueOf(xValsToArray[i]);
       
       return xValsArr;
    }
    
    /**
     * This method creates an Array of Y-Values that is sorted and cannot 
     * contain duplicates. 
     * 
     * @return A sorted, non-duplicated, array of integers containing the 
     * Y-Values of the points. 
     */
    public int[] getYValues()
    {
       // Convert the TreeMap of Y-Values to an array of Objects.
       Object[] yValsToArray = yVals.toArray();
       
       // Create an array of integers that is the same size as yValsToArray.
       int[] yValsArr = new int[yVals.size()];
       
       // Convert each object in yValsToArray to an int.
       for (int i = 0; i < yVals.size(); i++)
           yValsArr[i] = Integer.parseInt(String.valueOf(yValsToArray[i]));
       
       return yValsArr;
    }
    
    /**
     * This method clears the data inside the HashMap, ArrayList, and TreeMap.
     */
    public void clearData()
    {
        dataMap.clear();
        xVals.clear();
        yVals.clear();
    }
    
    /**
     * This method creates a dataset from the points created from the .csv file, 
     * utilized in creating a Bar Chart.
     * 
     * @param xMin The minimum X Value that is present in the chart.
     * @param xMax The maximum X Value that is present in the chart.
     * @return A DefaultCategoryDataset containing each point from the HashMap. 
     */
    public CategoryDataset createDataset(int xMin, int xMax)
    {
        // Create a new DefaultCategorySet.
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        
        // Convert the keys HashMap to a set of String.
        Set<String> keys = dataMap.keySet();
        
        /* Loop over each String in the keySet and add a new value to the 
        dataset. */
        for (int i = xMin; i <= xMax; i++)
        {
            // The current key.
            String key = String.valueOf(keys.toArray()[i]);
            
            // The list of points that are connected to the current key.
            ArrayList<Point> pointList = dataMap.get(key);
            
            /* If the key doesn't equal the x-value of the first point, loop 
            over each point and add it to the dataset. If the key does equal, 
            get the only point in the list's contents and add it to the 
            dataset.*/ 
            if (!key.equals(pointList.get(0).getX()))
            {
                for (int j = 0; j < pointList.size(); j++) 
                {
                    dataset.addValue(pointList.get(j).getY(), 
                        pointList.get(j).getX(), key);     
                }
            }
            else
            {
                dataset.addValue(pointList.get(0).getY(), 
                    pointList.get(0).getX(), pointList.get(0).getX());
            }
        }
        
        return dataset;
    }
    
    /**
     * This method creates a PieDataset
     * @return A PieDataset containing the data from the HashMap.
     */
    public PieDataset createPieDataset()
    {
        // Allocate memory for a new DefaultPieDataset.
        DefaultPieDataset dataset = new DefaultPieDataset();
        
        // Loop over every key from the keyset and calculate the percentage.
        for (String key : dataMap.keySet())
        {
            double value = 0.0;
            
            /* If the ArrayList that corresponds to the current key's size is 
            greater than 2, add the values of each element in that ArrayList. 
            If the size is not greater than 2, calculate the value of each 
            percentage and add it to the dataset. */
            if (dataMap.get(key).size() > 2)
            {
                for(int i = 0; i < dataMap.get(key).size(); i++)
                {
                    value += Double.valueOf(dataMap.get(key).get(i).getY()) 
                            / 100;
                }
                
                dataset.setValue(key, value);
            }
            else
            {
                value = Double.valueOf(dataMap.get(key).get(0).getY()) 
                        / 100;
                
                dataset.setValue(key, value);
            }
        }
        
        return dataset;
    }
}