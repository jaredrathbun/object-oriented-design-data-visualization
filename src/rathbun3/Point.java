package rathbun3;

/**
 * <pre>
 * Author: Jared Rathbun 
 * Course: CSC 2620 
 * Due Date: 10/7/2020
 *
 * This class contains the data for a point, which is has an X and Y coordinate.
 * </pre>
 */
public class Point
{
    private String x;
    private int y;

    /**
     * Constructor for the Point object.
     * 
     * @param x The x value.
     * @param y The y value.
     */
    public Point(String x, int y)
    {
        this.x = x;
        this.y = y;
    }
    
    /**
     * Getter for the X coordinate.
     * 
     * @return A String containing the X value. 
     */
    public String getX()
    {
        return x;
    }
    
    /**
     * Getter for the Y coordinate.
     * 
     * @return An int containing the Y value.
     */
    public int getY()
    {
        return y;
    }
}
