package rathbun3;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;

/**
 * <pre>
 * Author: Jared Rathbun 
 * Course: CSC 2620 
 * Due Date: 10/7/2020
 *
 * This class contains the instructions for the view (GUI) and code for creating
 * a JFreeChart specifically, a Bar Chart and Pie Chart.
 * </pre>
 */
public class ViewController extends JFrame
{
    // Components and Flags for the GUI.
    private final Model model;
    private ChartPanel chartPanel;
    private GridBagConstraints gbc;
    private JPanel actionsPanel, leftPanel, xyPanel;
    private JButton loadButton, saveButton, quitButton;
    private String chartTitle, yAxisTitle, xAxisTitle;
    private JTextField titleField, xAxisField, yAxisField;
    private JSlider xMinSlider, xMaxSlider, yMinSlider, yMaxSlider; 
    private JFreeChart barChart, pieChart;
    private boolean isBarChart, isLeftPanelVisible;
    private int yMin, yMax, xMin, xMax;
    private Color bgColor, borderColor;
    private JRadioButton barChartButton, pieChartButton;
    
    public ViewController(Model m, String title)
    {
        super(title);
        
        setLayout(new BorderLayout());
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(1400, 1000);
        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
        
        model = m;
        initComponents();
        initActionsPanel();

        validate();
    }
    
    /**
     * This method initializes the components of the frame and panels.
     */
    private void initComponents()
    {
        gbc = new GridBagConstraints();
        loadButton = new JButton("Load File");
        saveButton = new JButton("Save Chart");
        saveButton.setEnabled(false);
        quitButton = new JButton("Quit");
        actionsPanel = new JPanel();
        leftPanel = new JPanel();
        titleField = new JTextField(15);
        xAxisField = new JTextField(15);
        yAxisField = new JTextField(15);
        barChartButton = new JRadioButton("Bar Chart");
        pieChartButton = new JRadioButton("Pie Chart");
    }
    
    /**
     * This method initializes the actions panel which holds the load, save 
     * and quit buttons.
     */
    private void initActionsPanel()
    {
        // Set the layout of the panel to FlowLayout.
        actionsPanel.setLayout(new FlowLayout());
        
        // Add an ActionListener to the load button.
        loadButton.addActionListener((ActionEvent e) -> {
            
            /* If the left panel is visible, clear the data and remove the 
            existing chart from the GUI. */
            if (isLeftPanelVisible) 
            {
                model.clearData();
                remove(chartPanel);
            }
            
            // Read the file from the user.
            model.readFile();
            
            // If the left panel is visible, update just the sliders.
            if(!isLeftPanelVisible)
                initLeftPanel();
            else
                initSliders();
        });
        actionsPanel.add(loadButton);
        
        // Add an ActionListener to the save button.
        saveButton.addActionListener((ActionEvent e) -> {
            // Create a new JFileChooser and set the file filter.
            JFileChooser fc = new JFileChooser();
            fc.setFileFilter(new FileNameExtensionFilter("PNG Images", "png"));
            
            /* Save the image, and if it is successful show a message dialog 
            telling the user that. */
            if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
            {
                if (model.saveImage(fc.getSelectedFile(), getActiveChart()))
                    JOptionPane.showMessageDialog(rootPane, "File saved "
                            + "successfully.");
            }
        });
        actionsPanel.add(saveButton);
        
        // Add an ActionListener to the quit button.
        quitButton.addActionListener((ActionEvent e) -> {
            System.exit(0);
        });
        actionsPanel.add(quitButton);
        
        // Finally, add the panel to the frame.
        add(actionsPanel, BorderLayout.NORTH);
    }
    
    /**
     * This method initializes the left panel of the frame, which allows for the 
     * user to specify the axis labels, title, colors, and data ranges. 
     */
    private void initLeftPanel()
    {
        // The button for submitting the titles.
        JButton submitButton = new JButton("Create Chart");
        
        // Set the Layout and add a title to the panel.
        leftPanel.setLayout(new GridBagLayout());
        leftPanel.setBorder(BorderFactory.
                createTitledBorder("Chart Information"));
        
        // Create a panel for the titles of the chart and add labels.
        JPanel titlesPanel = new JPanel();
        titlesPanel.setLayout(new GridBagLayout());
        titlesPanel.setBorder(BorderFactory.createTitledBorder("Chart Titles"));
        
        // Add the title label.
        JLabel titleLabel = new JLabel("Title:  ");
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.LINE_END;
        titlesPanel.add(titleLabel, gbc);
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.gridx = 1;
        gbc.gridy = 1;
        titlesPanel.add(titleField, gbc);
        
        // Add the X-Axis Label.
        JLabel xAxisLabel = new JLabel("X-Axis Title:  ");
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.LINE_END;
        titlesPanel.add(xAxisLabel, gbc);
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.gridx = 1;
        gbc.gridy = 2;
        titlesPanel.add(xAxisField, gbc);
        
        // Add the Y-Axis Label.
        JLabel yAxisLabel = new JLabel("Y-Axis Title:  ");
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.LINE_END;
        titlesPanel.add(yAxisLabel, gbc);
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.gridx = 1;
        gbc.gridy = 3;
        titlesPanel.add(yAxisField, gbc);
        
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.CENTER;
        leftPanel.add(titlesPanel, gbc);
        
        // Initialize the Sliders.
        initSliders();
        
        // Initialize the Type and Colors panel.
        initTypeAndColors();
       
        // Add an action listener to the submit button.
        submitButton.addActionListener((ActionEvent e) -> {
            if (titleField.getText().equals("") || xAxisField.getText().
                    equals("") || yAxisField.getText().equals("") || 
                    (!barChartButton.isSelected() && 
                     !pieChartButton.isSelected()))
                JOptionPane.showMessageDialog(rootPane,
                        "Please fill in all fields.");
            else
            {
                chartTitle = titleField.getText();
                xAxisTitle = xAxisField.getText();
                yAxisTitle = yAxisField.getText();
                createChart();
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.anchor = GridBagConstraints.CENTER;
        leftPanel.add(submitButton, gbc);
        
        // Add the left panel to the GUI and validate.
        add(leftPanel, BorderLayout.WEST);
        validate();
        isLeftPanelVisible = true;
    }
    
    /**
     * This method initializes the panel that holds the Types and Colors for 
     * the chart.
     */
    private void initTypeAndColors()
    {
        // Create a new panel and add a title to it.
        JPanel typeAndColorPanel = new JPanel();
        typeAndColorPanel.setLayout(new GridBagLayout());
        typeAndColorPanel.setBorder(BorderFactory.
                createTitledBorder("Chart Type + Color Selections"));
        JLabel chartTypeLabel = new JLabel("Chart Type: ");
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_END;
        typeAndColorPanel.add(chartTypeLabel, gbc);
        
        // Add listeners to both RadioButtons.
        barChartButton.addActionListener((ActionEvent e) -> {
            isBarChart = true;
        });
        pieChartButton.addActionListener((ActionEvent e) -> {
            isBarChart = false;
        });
        ButtonGroup bGroup = new ButtonGroup();
        bGroup.add(barChartButton);
        bGroup.add(pieChartButton);
        
        /* Create a new panel for just the RadioButtons, and add it to the 
        typeAndColorPanel. */
        JPanel radioButtonsPanel = new JPanel();
        radioButtonsPanel.add(barChartButton);
        radioButtonsPanel.add(pieChartButton);
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.LINE_START;
        typeAndColorPanel.add(radioButtonsPanel, gbc);
        
        // Add a button to select both the background color and border color.
        JButton selectColorButton = new JButton("Select Background \nColor");
        selectColorButton.setBackground(Color.BLACK);
        selectColorButton.setForeground(Color.WHITE);
        selectColorButton.addActionListener((ActionEvent e) -> {
            bgColor = JColorChooser.showDialog(this, 
                    "Choose Background Color", getBackground());
            selectColorButton.setForeground(bgColor);
        });
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        typeAndColorPanel.add(selectColorButton, gbc);
        
        JButton selectGridColorButton = new JButton("Select Border Color");
        selectGridColorButton.setBackground(Color.BLACK);
        selectGridColorButton.setForeground(Color.WHITE);
        selectGridColorButton.addActionListener((ActionEvent e) -> {
            borderColor = JColorChooser.showDialog(this, "Select Border Color",
                    getBackground());
            selectGridColorButton.setForeground(borderColor);
        });
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.CENTER;        
        typeAndColorPanel.add(selectGridColorButton, gbc);
        
        // Add the typeAndColorPanel to leftPanel.
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        leftPanel.add(typeAndColorPanel, gbc);
    }
    
    /**
     * This method initializes the sliders to let the user select what ranges 
     * of the chart to view. 
     */
    private void initSliders()
    {
        /* If the left panel is visible, remove all of the components of the 
        xyPanel and remove the xyPanel from the leftPanel. */
        if (isLeftPanelVisible)
        {
            xyPanel.removeAll();
            leftPanel.remove(xyPanel);
        }   
        
        // Allocate memory for the panel and the Hashtables used for the sliders.
        xyPanel = new JPanel();
        Hashtable xLabelTable;
        Hashtable yLabelTable;
  
        // If the left panel is visible, reallocate the Hashtables.
        if (isLeftPanelVisible)
        {
            xLabelTable = new Hashtable();
            yLabelTable = new Hashtable();
        }
        
        // Set a layout and add a border to the panel.
        xyPanel.setLayout(new GridBagLayout());
        xyPanel.setBorder(BorderFactory.createTitledBorder("X/Y Ranges"));
        
        String[] xVals = model.getXValues();
        int xValsLength = xVals.length;
        xLabelTable = new Hashtable();
        
        // Populate the Hashtable for the xValues.
        for (int i = 0; i < xValsLength; i++)
            xLabelTable.put(i, new JLabel(xVals[i]));
        
        // Create JLabels for the X-Min and X-Max and add them to the panel.
        JLabel xMinLabel = new JLabel("<HTML><U>X-Min</U></HTML>");
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.CENTER;
        xyPanel.add(xMinLabel, gbc);
        JLabel xMaxLabel = new JLabel("<HTML><U>X-Max</U></HTML>");
        gbc.gridx = 1;
        gbc.gridy = 0;
        xyPanel.add(xMaxLabel, gbc);
        
        // Add the X-Min and X-Max Sliders to the GUI.
        xMinSlider = new JSlider(JSlider.VERTICAL, 0, xValsLength - 1, 0);
        xMinSlider.setLabelTable(xLabelTable);
        xMinSlider.setPaintLabels(true);
        xMinSlider.setPaintTicks(true);
        xMin = 0;
        xMinSlider.addChangeListener((ChangeEvent e) -> {
            xMin = ((JSlider) e.getSource()).getValue();
        });
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.insets = new Insets(10, 10, 10, 10);
        xyPanel.add(xMinSlider, gbc);
        xMaxSlider = new JSlider(JSlider.VERTICAL, 0, xValsLength - 1, 
                xValsLength - 1);
        xMaxSlider.setLabelTable(xLabelTable);
        xMaxSlider.setPaintLabels(true);
        xMaxSlider.setPaintTicks(true);
        xMax = xValsLength - 1;
        xMaxSlider.addChangeListener((ChangeEvent e) -> {
            xMax = ((JSlider) e.getSource()).getValue();
        });
        gbc.gridx = 1;
        gbc.gridy = 1;
        xyPanel.add(xMaxSlider, gbc);
        
        // Populate the Y-Values Hashtable.
        int[] yVals = model.getYValues();
        int yValsLength = yVals.length;
        yLabelTable = new Hashtable();
        for (int i = 0; i < yValsLength; i++)
            yLabelTable.put(i, new JLabel(String.valueOf(yVals[i])));
        
        // Create JLabels for the Y-Min and Y-Max and add them to the panel.
        JLabel yMinLabel = new JLabel("<HTML><U>Y-Min</U></HTML>");
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.LINE_END;
        xyPanel.add(yMinLabel, gbc);
        JLabel yMaxLabel = new JLabel("<HTML><U>Y-Max</U></HTML>");
        gbc.gridx = 0;
        gbc.gridy = 3;
        xyPanel.add(yMaxLabel, gbc);
        
        // Add the Y-Min and Y-Max Sliders to the GUI.
        yMinSlider = new JSlider(JSlider.HORIZONTAL, 0, yValsLength - 1, 
                0);
        yMinSlider.setLabelTable(yLabelTable);
        yMinSlider.setPaintLabels(true);
        yMinSlider.setMajorTickSpacing(1);
        yMinSlider.setPaintTicks(true);
        yMin = 0;
        yMinSlider.addChangeListener((ChangeEvent e) -> {
            yMin = yVals[((JSlider) e.getSource()).getValue()];
        });
        gbc.gridx = 1;
        gbc.gridy = 2;
        xyPanel.add(yMinSlider, gbc);
        yMaxSlider = new JSlider(JSlider.HORIZONTAL, 0, yValsLength - 1, 
               yValsLength - 1);
        yMaxSlider.setLabelTable(yLabelTable);
        yMaxSlider.setPaintLabels(true);
        yMaxSlider.setMajorTickSpacing(1);
        yMaxSlider.setPaintTicks(true);
        yMax = yValsLength - 1;
        yMaxSlider.addChangeListener((ChangeEvent e) -> {
            yMax = yVals[((JSlider) e.getSource()).getValue()];
        });
        gbc.gridx = 1;
        gbc.gridy = 3;
        xyPanel.add(yMaxSlider, gbc);
        
        // Add the xyPanel to the leftPanel.
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.CENTER;
        leftPanel.add(xyPanel, gbc);
        
        validate();
    }
    
    /**
     * This method gets the chart that is active.
     * 
     * @return The current chart. 
     */
    private JFreeChart getActiveChart()
    {
        if (isBarChart)
            return barChart;
        return pieChart;
    }
    
    /**
     * This method creates a chart, switching depending on the isBarChart flag.
     */
    private void createChart()
    {        
        /* If isBarChart is true, create a Bar Chart, otherwise create a 
        Pie Chart. */
        if (isBarChart)
        { 
            barChart = ChartFactory.createBarChart(chartTitle, xAxisTitle,
                yAxisTitle, model.createDataset(xMin, xMax), 
                PlotOrientation.VERTICAL, true, true, false);
            barChart = setAxisRestrictions(barChart);
            barChart.setBorderPaint(borderColor);
            barChart.setBorderStroke(new BasicStroke(10));
            barChart.setBorderVisible(true);
            barChart.setBackgroundPaint(bgColor);
            
            chartPanel = new ChartPanel(barChart);
        }
        else
        {
            pieChart = ChartFactory.createPieChart(chartTitle, 
                    model.createPieDataset());
            pieChart.setBorderPaint(borderColor);
            pieChart.setBorderVisible(true);
            pieChart.setBorderStroke(new BasicStroke(10));
            pieChart.setBackgroundPaint(bgColor);
                
            chartPanel = new ChartPanel(pieChart);
        }
        saveButton.setEnabled(true);
        
        // Add the ChartPanel to the panel.
        add(chartPanel, BorderLayout.CENTER);
        validate();
    }
    
    /**
     * This method Y values that are visible on the 
     * chart from the sliders implemented on the GUI.
     * 
     * @param chart The chart to change the ranges of.
     * @return The same chart with the Y restrictions.
     */
    private JFreeChart setAxisRestrictions(JFreeChart chart)
    {   
        // Set the minimum and maximum Y values.
        chart.getCategoryPlot().getRangeAxis().setLowerBound(yMin);
        chart.getCategoryPlot().getRangeAxis().setUpperBound(yMax);
 
        return chart;
    }
}